var express = require('express');
var app = express();
var staticDir = __dirname + "/public"
var fs = require("fs");

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(express.static(staticDir));

var router = express.Router();

router.get('/', function (req, res) {
    console.log("Sending Soccer Street");
    res.sendFile(staticDir + "/soccerStreet.html");
});

app.use("/", router);

var server = app.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('listening at port 3000');
});


var mongoose = require('mongoose'),
    User = require('./user-model');

var connStr = "mongodb://localhost:27017/mongoose-bcrypt-test";
mongoose.connect(connStr, function(err){
	if(err) throw err;
	else console.log("Connection Success");
});

// ROUTES FOR OUR API
// =============================================================================
var apiRouter = express.Router();              // get an instance of the express Router

// middleware to use for all requests
apiRouter.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
apiRouter.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });   
});

app.use('/api',apiRouter);

apiRouter.route('/users')
	.post(function(request,res){
		console.log(request.body);
		var user = new User({
			name : request.body.name,
			username : request.body.username,
			password : request.body.password
		});

		user.save(function(err){
			if(err){

				console.log(err);
				res.json({ERROR_MESSAGE : "Unable to create User"});
			}
			else
				res.json(user);
		})
		
	})
	.get(function(req,res){
		 var username  = req.query.username;
		 var password = req.query.password;
		 console.log("Looking for " + username + " " + password);
		 User.find({'username' : username, 'password' : password}, function(err, user){
		 	if(user.length > 0)
		 		res.json(user[0]); 
	 		else
	 			res.json({ERROR_MESSAGE : "WRONG USERNAME/PASSWORD"});	
		 });
	});

apiRouter.route('/user/update')
	.post(function(req,res){
		var username = req.body.username;
		var user = req.body.user;
		console.log(user);
		User.findOne({'username' : username}, function(err, newuser){
			if(err){
				console.log(err);
				res.json({ERROR_MESSAGE : "Unable to update"});
			}
			else{
				newuser.preferences = user.preferences;
				newuser.save(function(err){
					if(!err){
						res.json(newuser);
					}else
						res.json({ERROR_MESSAGE : "Unable to update"});
				});
			}
		});
	})



  
// // create a new user called chris
// var chris = new User({
//   name: 'Chris',
//   username: 'tanmayv',
//   password: 'password',
//   preferences : {
//   	competitions : [123,213,421]
//   }
// });

// // call the custom method. this will just add -dude to his name
// // user will now be Chris-dude


// // call the built-in save method to save to the database
// chris.save(function(err) {
//   if (err) throw err;

//   console.log('User saved successfully!');
// });

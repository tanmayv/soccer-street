var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var userSchema = new Schema({
  name: String,
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  preferences : {
    competitions : {type : Array},
    teams : {type : Array}
  }
});

userSchema.methods.comparePassword = function(passwd){
	if (this.password == password)
		return true;
	else
		return false;
}
// the schema is useless so far
// we need to create a model using it
var User = mongoose.model('User', userSchema,'TheUser');

// make this available to our users in our Node applications
module.exports = User;
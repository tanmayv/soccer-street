var soccerApp = angular.module("SoccerApp");


soccerApp.factory("SoccerFactory", function($http){
	var serverUrl = "https://api.crowdscores.com/api/v1"
	var api_key = "4607b1fc0da8440fa1a6a7ff0811058b"
    var mongoApiUrl = "http://localhost:3000/api"
	return {
        login : function(username, password){
            var req = {
                method: 'GET',
                url: mongoApiUrl + "/users",
                headers: {
                    'Content-Type': "application/json"
                },
                params: {
                    'username' : username,
                    'password' : password
                }
            }
            return $http(req);  
        },
        updateUser : function( user){
            var req = {
                method: 'POST',
                url: mongoApiUrl + "/user/update",
                headers: {
                    'Content-Type': "application/json"
                },
                data: {
                    'username' : user.username,
                    'user' : user
                }
            }
            return $http(req);  
        },
        signUp : function(name,username, password){
            var req = {
                method: 'POST',
                url: mongoApiUrl + "/users",
                headers: {
                    'Content-Type': "application/json"
                },
                data: {
                    'name' : name,
                    'username' : username,
                    'password' : password
                }
            }
            return $http(req); 
        },

		getTeams : function(roundId, competitionId){
			 var req = {
                method: 'GET',
                url: serverUrl + "/teams",
                headers: {
                    'Content-Type': "application/json"
                },
                params: {
                    round_ids : roundId,
                    competition_ids : competitionId,
                    'api_key' : api_key
                }
            }
            return $http(req);
		},

        getTeam : function(teamId){
             var req = {
                method: 'GET',
                url: serverUrl + "/teams/" + teamId,
                headers: {
                    'Content-Type': "application/json"
                },
                params: {
                    'api_key' : api_key
                }
            }
            return $http(req);
        },

        getMatches : function(teamId, roundId, competitionId, from, to){
             var req = {
                method: 'GET',
                url: serverUrl + "/matches",
                headers: {
                    'Content-Type': "application/json"
                },
                params: {
                    round_ids : roundId,
                    competition_id : competitionId,
                    team_id : teamId,
                    'from' : from,
                    'to' : to,
                    'api_key' : api_key
                }
            }
            return $http(req);
        },

        getMatch: function(matchId){
             var req = {
                method: 'GET',
                url: serverUrl + "/matches/" + matchId,
                headers: {
                    'Content-Type': "application/json"
                },
                params: {
                    'api_key' : api_key
                }
            }
            return $http(req);
        },

        getCompetitions : function(){
             var req = {
                method: 'GET',
                url: serverUrl + "/competitions",
                headers: {
                    'Content-Type': "application/json"
                },
                params: {
                    'api_key' : api_key
                }
            }
            return $http(req);
        },

        getRounds : function(competitionId){
             var req = {
                method: 'GET',
                url: serverUrl + "/rounds",
                headers: {
                    'Content-Type': "application/json"
                },
                params: {
                    competition_ids : competitionId,
                    'api_key' : api_key
                }
            }
            return $http(req);
        },

        getLeagueTable: function(teamId,roundId, competitionId){
             var req = {
                method: 'GET',
                url: serverUrl + "/league-tables",
                headers: {
                    'Content-Type': "application/json"
                },
                params: {
                    team_id : teamId,
                    round_id: roundId,
                    competition_id : competitionId,
                    'api_key' : api_key
                }
            }
            return $http(req);
        },

        getPlayerStats: function(teamId,roundId, competitionId){
             var req = {
                method: 'GET',
                url: serverUrl + "/playerstats",
                headers: {
                    'Content-Type': "application/json"
                },
                params: {
                    team_ids : teamId,
                    round_ids: roundId,
                    competition_ids: competitionId,
                    'api_key' : api_key
                }
            }
            return $http(req);
        }
	}

});

soccerApp.factory('instagram', ['$http',
    function($http) {
      return {
        fetchPopular: function(callback) {

          var endPoint = "https://api.instagram.com/v1/media/popular?client_id=642176ece1e7445e99244cec26f4de1f&callback=JSON_CALLBACK";

          $http.jsonp(endPoint).success(function(response) {
            callback(response.data);
          });

          
        }
      }
    }
  ])
var app = angular.module("http-loader", []);

app.directive('httpLoading', ['$http', function ($http) {
    return {
        link: function (scope, elm, attrs) {
            scope.isLoading = function () {
                console.log($http.pendingRequests.length + " Length");
                return $http.pendingRequests.length > 0;
            }

            scope.$watch(scope.isLoading, function (v) {
                if (v)
                    elm.show();
                else
                    elm.hide();
            });
        }
    }
    }]);

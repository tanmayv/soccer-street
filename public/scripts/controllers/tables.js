var soccerApp = angular.module("SoccerApp");

soccerApp.controller('TableCtrl', function($scope, SoccerFactory){
	SoccerFactory.getCompetitions().then(
        function(response){
            console.log(response.data);
            $scope.data = response.data;
            $scope.selectedCompetition = $scope.data[0];
        }
    );

    $scope.tablesData = [];

    $scope.$watch(function(){
    	return $scope.selectedCompetition;	
    }, function(newValue){
    	console.log("Competition switced to " + newValue);
    	if(newValue){
	    	SoccerFactory.getLeagueTable(null, null,newValue.dbid).then(
	    			function(response){
	    				console.log("Response from LeagueTable")
	    				$scope.tablesData = response.data;
	    				console.log(response.data);	    			}
	    		);
    	}
    })


});
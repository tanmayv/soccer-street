var soccerApp = angular.module("SoccerApp");

soccerApp.controller('PlayerCtrl', function(SoccerFactory,$scope){
    $scope.data = SoccerFactory.getCompetitions().then(
        function(response){
            console.log(response.data);
            $scope.data = response.data;
            $scope.selectedCompetition = $scope.data[0];
        }
    );

    $scope.players = [];

    $scope.$watch(function(){
          return $scope.selectedCompetition;
    },function(newValue){
    		console.log("Competition switced to " + newValue);
    	if(newValue){
	    	SoccerFactory.getPlayerStats(null,null, newValue.dbid).then(
	    			function(response){
	    				$scope.players = response.data;
	    			}
	    		);
    }
    });
})
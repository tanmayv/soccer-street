var soccerApp = angular.module("SoccerApp");

soccerApp.controller('FixtureCtrl', function(SoccerFactory,$scope, instagram){
    SoccerFactory.getCompetitions().then(
        function(response){
            console.log(response.data);
            $scope.data = response.data;
            $scope.selectedCompetition = $scope.data[0];
        }
    );

    $scope.teams = [];
    $scope.tables = [];
    $scope.$watch(function(){
    	return $scope.selectedCompetition;	
    }, function(newValue){
    	console.log("Competition switced to " + newValue);
    	if(newValue){
	    	SoccerFactory.getMatches(null, null, newValue.dbid).then(
	    			function(response){
                        var matches = response.data;
                        var today = new Date();
                        for(var i =0 ; i < matches.length; i++){
                            var date = new Date(matches[i].start);
                            if(date.getDate() >= today.getDate() && date.getMonth() >= today.getMonth() && date.getYear() >= today.getYear())
                                $scope.tables.push(matches[i]);
                        }
	    				
	    			}
	    		);
    	}
    })

})

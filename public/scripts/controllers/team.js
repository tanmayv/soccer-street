var soccerApp = angular.module("SoccerApp");

soccerApp.controller('TeamsCtrl', function(SoccerFactory,$scope, instagram){
    var self = this;
    SoccerFactory.getCompetitions().then(
        function(response){
            console.log(response.data);
            $scope.data = response.data;
            var data = response.data;
            $scope.selectedCompetition = $scope.data[0];
        }
    );

    $scope.teams = [];

    $scope.$watch(function(){
    	return $scope.selectedCompetition;	
    }, function(newValue){
    	console.log("Competition switced to " + newValue);
    	if(newValue){
	    	SoccerFactory.getTeams(null, newValue.dbid).then(
	    			function(response){
	    				$scope.teams = response.data;
	    			}
	    		);
    	}
    })

})

soccerApp.controller("TeamCtrl", function($scope, SoccerFactory, $routeParams, TwitterApi){
    $scope.teamid = $routeParams.teamid;
    SoccerFactory.getTeam($scope.teamid).then(
            function(response){
                $scope.data = response.data;
            }
        );
    $scope.hidePlayer = true;

    SoccerFactory.getMatches($scope.teamid).then(
            function(response){
                $scope.teamMatches = response.data;
                console.log(response.data);
            }
        );
    
    $scope.togglePlayerList = function(){
        $scope.hidePlayer = !$scope.hidePlayer;
    }
});
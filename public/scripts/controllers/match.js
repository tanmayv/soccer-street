var soccerApp=angular.module("SoccerApp");

soccerApp.controller('MatchesCtrl', function(SoccerFactory, $scope){

    $scope.yesterday = [];
    $scope.today = [];
    $scope.tomorrow = [];
    var today = new Date();
    var tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
	var yesterday = new Date(new Date().getTime() - 24 * 60 * 60 * 1000);
	var to =  tomorrow.toISOString();
	var from = yesterday.toISOString();
	SoccerFactory.getMatches(null,null,null,from,to).then(
	    			function(response){
	    				var matches = response.data;
	    				for(var i =0 ; i < matches.length; i++){
	    					var date = new Date(matches[i].start);
	    					if(date.getDate()  < today.getDate() || date.getMonth() < today.getMonth()){
	    						$scope.yesterday.push(matches[i]);
	    					}
	    					else if( date.getDate() > today.getDate()){
	    						$scope.tomorrow.push(matches[i]);
	    					}
	    					else{
	    						var diff = today.getHours() - date.getHours();
	    						if(diff <= 2 && diff >= 0){
	    							matches[i].live = true;
	    							console.log("LIVESHIT")
	    						}
    							$scope.today.push(matches[i]);
	    					}
	    				}
	    			}
	    		);
});

soccerApp.controller("MatchCtrl", function(SoccerFactory, $scope, $routeParams){
	$scope.matchid=$routeParams.matchid
	SoccerFactory.getMatch($scope.matchid).then(
			function(response){	
				$scope.data = response.data;
			}, function(response){}
		);
});
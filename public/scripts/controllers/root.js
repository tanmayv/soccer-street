var soccerApp = angular.module("SoccerApp");

soccerApp.controller('RootCtrl', function($scope, $location,$rootScope, $sessionStorage){
    $scope.redirectTo = function(path){
        $location.path(path);
    }

    $scope.msToDate = function(ms){
    	var date  = new Date(ms);
    	console.log(date);
    	var dateString = date.toString("MMM dd, yyyy");
        return dateString.substring(0,dateString.indexOf("GMT"));

    }

    $scope.logoutUser = function(){
        $sessionStorage.loggedIn = false;
        $sessionStorage.loggedInUser = null;
        $rootScope.loggedIn = false;
        $rootScope.loggedInUser = null;
        $location.path("login");
    }

    if($sessionStorage.loggedIn){
    	$rootScope.loggedIn = true;
    	$rootScope.loggedInUser = JSON.parse($sessionStorage.loggedInUser);
    }else
        $location.path("login");


})
var soccerApp = angular.module("SoccerApp");

soccerApp.controller('HomeCtrl',function($scope, $rootScope, SoccerFactory){
    $scope.preferredMatches = [];
    $scope.preferredTeamPastMatches = [];
    $scope.preferredTeamFutureMatches = [];
    
    if($rootScope.loggedIn){
    	var preferences = $rootScope.loggedInUser.preferences;
    	var todayDate = new Date();
    	var from = todayDate.toISOString();
    	var tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
    	var pastWeek = new Date(new Date().getTime() - 7*24 * 60 * 60 * 1000);
    	var to =  tomorrow.toISOString();
    	var pastWeekFrom = pastWeek.toISOString();
    	for(var i = 0; i < preferences.teams.length; i++){
    		SoccerFactory.getMatches(preferences.teams[i],null, null,pastWeekFrom,from).then(
    				function(response){
    					var length = response.data.length;
    					$scope.preferredTeamPastMatches = $scope.preferredTeamPastMatches.concat(response.data.slice(length-2,length));
    				}
    			);
    	}
    	for(var i = 0; i < preferences.competitions.length; i++){
    		SoccerFactory.getMatches(null,null, preferences.competitions[i],from).then(
    				function(response){
    					$scope.preferredMatches = $scope.preferredMatches.concat(response.data.slice(0,4));
    				}
    			);
    	}
        for(var i = 0; i < preferences.teams.length; i++){
            SoccerFactory.getMatches(preferences.teams[i],null, null,from).then(
                    function(response){
                        var length = response.data.length;
                        $scope.preferredTeamFutureMatches = $scope.preferredTeamFutureMatches.concat(response.data.slice(0,2));
                    }
                );
        }
    }
});
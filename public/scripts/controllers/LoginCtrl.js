var soccerApp = angular.module("SoccerApp");

soccerApp.controller("LoginCtrl", function(SoccerFactory,$scope, $location,$rootScope, $sessionStorage){
    var self = this;
    $scope.signup = false;
    $scope.user = {};
    $scope.shittyname = false;
    $scope.userPrefs = {};
    $scope.selectedCompetitions = [];
    $scope.selectedTeams = [];
    $scope.toggleSignup = function(){
        $scope.signup = !$scope.signup;
    }

    $scope.signUp = function(){ 
        console.log("selectedTeams " + $scope.userPrefs.competitions);
        console.log("selectedCompetitions " + $scope.userPrefs.teams);
        for(var i = 0; i < $scope.userPrefs.competitions.length; i++){
            $rootScope.loggedInUser.preferences.competitions.push($scope.userPrefs.competitions[i].dbid);
        }     
        for(i = 0; i < $scope.userPrefs.teams.length; i++){
            $rootScope.loggedInUser.preferences.teams.push($scope.userPrefs.teams[i].dbid);
        }  
        console.log( $rootScope.loggedInUser);
        SoccerFactory.updateUser($rootScope.loggedInUser).then(
                function(response){
                    console.log(response.data);
                    $location.path("/home");
                }
            );
    }
    $scope.teams=[];
    $scope.preSignUp = function(){
        var name = $scope.user.name;
        var username = $scope.user.username;
        var password = $scope.user.password;

        if(name && username && password){
            SoccerFactory.signUp(name,username,password).then(function(response){
                    var user = response.data;
                    $sessionStorage.loggedIn = true;
                    $sessionStorage.loggedInUser = JSON.stringify(user);
                    $rootScope.loggedIn = true;
                    $rootScope.loggedInUser = user;
                    console.log(user);
                    $scope.shittyname = true;
                });   
        }

        
        SoccerFactory.getCompetitions().then(
            function(response){
                console.log(response.data);
                $scope.competitions = response.data;
            }
        );
        SoccerFactory.getTeams(null,2).then(
            function(response){
                console.log(response.data);
                $scope.teams = $scope.teams.concat(response.data);
            }
        );
        SoccerFactory.getTeams(null,46).then(
            function(response){
                console.log(response.data);
                $scope.teams = $scope.teams.concat(response.data);
            }
        );
    }


    $scope.loginUser = function(){
        var username = $scope.user.username;
        var password = $scope.user.password;
        console.log("Log in " + username + password)
        if(username && password){
           SoccerFactory.login(username,password).then(
                function(response){
                    if(response.data.username){
                        $sessionStorage.loggedIn = true;
                        $sessionStorage.loggedInUser = JSON.stringify(response.data);
                        $rootScope.loggedIn = true;
                        $rootScope.loggedInUser  = response.data;
                        $location.path("/home");
                    }
                }, function(response){}
            );
        }
    }
});
var soccerApp = angular.module("SoccerApp",['ngRoute','oi.select', 'http-loader','ngTwitter','ngStorage']);

soccerApp.config(['$routeProvider', function($routeProvider){
    $routeProvider.
        when('/home',{
            templateUrl : 'templates/home.html',
            controller : 'HomeCtrl'
        }).
        when('/team',{
            templateUrl : "templates/teams.html",
            controller : "TeamsCtrl"
        }).
        when('/team/:teamid',{
            templateUrl : "templates/team.html",
            controller : "TeamCtrl"
        }).
        when('/player',{
            templateUrl : "templates/player.html",
            controller : "PlayerCtrl"
        }).
        when('/fixtures',{
            templateUrl : "templates/Fixtures.html",
            controller  : "FixtureCtrl"
        }).
        when('/tables',{
            templateUrl : "templates/tables.html",
            controller : "TableCtrl"
        }).
        when('/match',{
            templateUrl : "templates/matches.html",
            controller : "MatchesCtrl"
        }).
        when('/match/:matchid',{
            templateUrl : "templates/match.html",
            controller : "MatchCtrl"
        }).
        when('/competition',{
            templateUrl : "templates/competition.html",
            controller : "CompCtrl"
        }).
        when('/login',{
            templateUrl : "templates/loginForm.html",
            controller : "LoginCtrl"
        }).
        otherwise({
            redirectTo : '/home'
        });
}]);